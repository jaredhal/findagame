import { Component, Input } from '@angular/core';

import { friendData, gamesListResponse, gamesList, gameResponse, SteamApiService } from './steam-api.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'findagame';

    friendsList: Array<friendData> = [];
    gameProfiles: Record<string, Array<gameResponse>> = {};
    sharedGames: Set<gameResponse> = new Set();
    reset: boolean = false;

    constructor(private steamAPI: SteamApiService) { }

    mergeGames(newGamesList: gamesList): void {
        if (this.sharedGames.size == 0 && !this.reset) {
            this.sharedGames = new Set(newGamesList);
            this.reset = true;
        } else {
            let tempSharedGames = this.sharedGames;
            for (let s of tempSharedGames) {
                let sharedIndex = newGamesList.findIndex(e => e.appid === s.appid);
                if (sharedIndex === -1) {
                    tempSharedGames.delete(s);
                }
            }
            this.sharedGames = tempSharedGames;
        }
    }

    searchGames(friend: friendData): void {
        console.log("finding games for friend:", friend.username);
        const gameSubscription = this.steamAPI.searchGames(friend.steamid).subscribe(
            (data: gamesListResponse) => {
                console.log("searchGames got this data:", data);
                console.log("games are:", data.response.games);
                this.mergeGames(data.response.games);
            }
        );
    }

    addFriend(friend: friendData): void {
        console.log("adding friend:", friend);
        let prexisting = this.friendsList.find(e => e.steamid === friend.steamid);
        if (!prexisting) {
            this.friendsList.push(friend);
            this.searchGames(friend);
        }
    }
} 
