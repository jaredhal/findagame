import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';

import { AppComponent } from './app.component';
import { FriendsListDisplayComponent } from './friends-list-display/friends-list-display.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SharedGamesComponent } from './shared-games/shared-games.component';

@NgModule({
    declarations: [
        AppComponent,
        FriendsListDisplayComponent,
        SearchBarComponent,
        SharedGamesComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatInputModule,
        MatCardModule,
        MatChipsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
