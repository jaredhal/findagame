import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendsListDisplayComponent } from './friends-list-display.component';

describe('FriendsListComponent', () => {
    let component: FriendsListDisplayComponent;
    let fixture: ComponentFixture<FriendsListDisplayComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [FriendsListDisplayComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(FriendsListDisplayComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
