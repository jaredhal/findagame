import { Component, OnInit, Input } from '@angular/core';

import { friendData } from '../steam-api.service';

@Component({
    selector: 'friends-list-display',
    templateUrl: './friends-list-display.component.html',
    styleUrls: ['./friends-list-display.component.scss']
})
export class FriendsListDisplayComponent implements OnInit {

    @Input() friendsList: Array<friendData> = [];

    constructor() { }

    ngOnInit(): void { }

}
