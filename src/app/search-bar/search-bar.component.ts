import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { pipe, map, mergeMap } from 'rxjs';

import { SteamApiService, searchUsernameResponse, friendData } from '../steam-api.service';

@Component({
    selector: 'search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

    @Output() newFriendEvent = new EventEmitter<friendData>();

    constructor(private steamAPI: SteamApiService) { }

    ngOnInit(): void { }

    searchUsername(name: string): void {
        console.log("searching username:", name);
        const searchSubscription = this.steamAPI.searchUsername(name).subscribe(
            (data: searchUsernameResponse) => {
                if (data.response.message) {
                    console.log("response message:", data.response.message);
                } else if (data.response.success != 0) {
                    let sid = data.response.steamid as string;
                    let friend = { username: name, steamid: sid };
                    this.newFriendEvent.emit(friend);
                } else {
                    console.log("unsuccessful search");
                    console.log(data);
                }
            }
        );
    }
}
