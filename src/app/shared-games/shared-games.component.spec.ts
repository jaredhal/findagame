import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedGamesComponent } from './shared-games.component';

describe('SharedGamesComponent', () => {
  let component: SharedGamesComponent;
  let fixture: ComponentFixture<SharedGamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharedGamesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SharedGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
