import { Component, OnInit, Input } from '@angular/core';
import { gameResponse } from '../steam-api.service';

@Component({
    selector: 'shared-games',
    templateUrl: './shared-games.component.html',
    styleUrls: ['./shared-games.component.scss']
})
export class SharedGamesComponent implements OnInit {

    @Input() sharedGames: Set<gameResponse> = new Set;

    constructor() { }

    ngOnInit(): void { }
}
