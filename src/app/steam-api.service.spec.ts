import { TestBed } from '@angular/core/testing';

import { SteamApiServiceService } from './steam-api-service.service';

describe('SteamApiServiceService', () => {
  let service: SteamApiServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SteamApiServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
