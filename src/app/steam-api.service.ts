import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';

export interface searchUsernameResponse {
    response: {
        success: number;
        steamid?: string;
        message?: string;
    }
};

export interface friendData {
    steamid: string;
    username: string;
};

export interface gameResponse {
    appid: string;
    name: string;
    img_icon_url: string;
    has_community_visible_stats: boolean;
    playtime_forever: number;
    playtime_windows_forever?: number;
    playtime_mac_forever?: number;
    playtime_linux_forever?: number;
}

export type gamesList = Array<gameResponse>;

export interface gamesListResponse {
    response: {
        game_count: number;
        games: gamesList;
    }
}

@Injectable({
    providedIn: 'root'
})
export class SteamApiService {

    private URLS = {
        usernameURL: `${environment.backend}/searchUsername/`,
        gamesURL: `${environment.backend}/searchGames/`,
    };

    constructor(private http: HttpClient) { }

    searchUsername(name: string): Observable<searchUsernameResponse> {
        return this.http.get<searchUsernameResponse>(this.URLS.usernameURL + name);
    }

    searchGames(userid: string): Observable<gamesListResponse> {
        return this.http.get<gamesListResponse>(this.URLS.gamesURL + userid);
    }
}
